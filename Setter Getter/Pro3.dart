class Demo{

  int? _x;
  String? str;
  double? _sal;

  Demo(this._x,this.str,this._sal);

  void setx(int x){

    _x=x;
  }

  void setNm(String nm){

    str=nm;
  }

  void setSal(double sal){

    _sal=sal;
  }

  get getX=> _x;
  get getst=> str;
  get getsal=> _sal;
  
}