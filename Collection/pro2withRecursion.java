class pro2withRecursion{
	
	public static void main(String[] args) {
		
		int arr[]=new int[]{7,3,9,11,4,2,5,6};
		
		int n=1;
		
		SelectionSort(arr,n);
		
	for(int i=0;i<arr.length;i++) {
		
		System.out.print(arr[i]+" ");

	}		
	}

	private static void SelectionSort(int[] arr,int n) {
		
		if(n==arr.length) {
			return;
		}
		
		int element=arr[n];
		int j=n-1;
		
		while(j>=0 && arr[j]>element) {
			
			arr[j+1]=arr[j];
			j--;
		}
		
		arr[j+1]=element;
		SelectionSort(arr,n+1);
		
	}
}