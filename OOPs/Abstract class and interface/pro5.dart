mixin DemoParent{

void m1(){

  print("Mixin m1");
}
}

class Demo{

  void m1(){

    print("m1 method"); 
  }
}

class DemoChild extends Demo with DemoParent{


}

void main(){

  DemoChild obj=new DemoChild();

  obj.m1();
  // obj.m2();

}

