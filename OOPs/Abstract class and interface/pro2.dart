abstract class Demo{

void m1(){

  print("m1 method");
}

void m2(){

  print("m2 Method");
}

}
class CDemo implements Demo{

  void m1(){
     
     print("Cdemo m1");
  }

  void m2(){
    print("Cdemo m2");
  }

}
void main(){

CDemo obj=new CDemo();
obj.m1();
obj.m2();

}