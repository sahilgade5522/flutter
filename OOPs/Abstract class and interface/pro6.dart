mixin DemoParent{

void m1(){

  print("Mixin m1");
}
}

mixin Demo{

  void m1(){

    print("m1 method"); 
  }
}
class DemoChild  with DemoParent,Demo{


}

void main(){

  DemoChild obj=new DemoChild();

  obj.m1();

}