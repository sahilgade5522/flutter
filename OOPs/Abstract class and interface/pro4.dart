abstract class InterfaceDemo1{

  void m1(){
    print("In m1 method");
  }
}

abstract class InterfaceDemo2{

  void m1(){
    print("In m2 method");
  }
}

class Demo implements InterfaceDemo1,InterfaceDemo2{

  void m1(){
    print("m1 Demo");
  }

  // void m2(){

  //   print("m2 Demo");
  // }
}

void main(){

  Demo obj=new Demo();
  obj.m1();
  // obj.m2();
}
