abstract class IFC{

  void material(){
    print("American Material");
  }

  void taste();

}
class IndFC implements IFC{

  void taste(){

    print("Indian taste");
  }

  void material(){

    print("Indian Material");
  }
}

class EUFC extends IFC{

  void taste(){
    print("Europian taste");

  }

  void material(){

    print("Europian Material");
  }
}

void main(){
  IndFC obj=new IndFC();
  obj.material();
  obj.taste();

  EUFC obj2=new EUFC();
  obj2.material();
  obj2.taste();
  
}