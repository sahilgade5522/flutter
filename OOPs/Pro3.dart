class Demo{

//WAYS OF WRITING CONSTRUCTOR;
  int? x;
  String? y;

  Demo({this.x,this.y});               //named parameter
  //Demo(this.x,this.y)               //simple way of Writing Constructor
  //Demo(this.x,[this.y="rajesh"]);  //Optional Parameter;
  //Demo(this.x,{this.y="raj"});    //default parameter

void printData(){

  print(x);
  print(y);
}
}
void main(){

  Demo d=new Demo(y:"dharmesh",x:90);
   Demo d1=new Demo(x:50,y:"Ramesh");
   d.printData();
   d1.printData();

}
