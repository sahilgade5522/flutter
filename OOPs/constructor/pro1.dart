class Company{

int? empCount;
String? compName;

//Company(this.empCount,this.compName);//normal const
// Company(this.empCount,[this.compName="deva"]);//optional const

// Company(this.empCount,{this.compName="dushman"});//default const
Company({this.empCount,this.compName});

}
void main(){

Company obj=new Company(empCount:600,compName: "rrr");

print(obj.empCount);
print(obj.compName);

}