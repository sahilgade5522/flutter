class Parent{

int x=10;
String str="name";

void ProParent(){

  print(x);
  print(str);
}

}
class Child extends Parent{

  int x=90;
  String str="data";

void ProChild(){

  print(x);
  print(str);
}

}
void main(){

Child obj=new Child();
obj.ProParent();
print(obj.x);
print(obj.str);

obj.ProChild();
print(obj.x);
print(obj.str);

}