class employee{

 final int? empCount;
  final String? empName;

  // employee(){                        //NO-Args Constructor

  //   print("In no args constructor");
  // }

  // employee(this.empCount,this.empName);     //Parameterized Constructor

  // employee.d(this.empCount,{this.empName="tan"});  //Named Constructor

const employee(this.empCount,this.empName);  //Constant Constructor 

  void printData(){

  print(empCount);
  print(empName);
}

}

void main(){

  employee ep=new employee(10,"sahil");
  ep.printData();

}

  //  employee ep2=new employee.d(90);
  // ep2.printData();

//    employee ep3=new employee(90,"Rj");
//   ep3.printData();
// 
