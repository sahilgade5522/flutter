class Demo{

  Demo._private(){
    print("in Demo Constructor");
  }

  factory Demo(){

    return Demo._private();
  }

  void fun(){

    print("In fun");
  }
}