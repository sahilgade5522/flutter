class backendLang{

String? lang;

  backendLang._private(String lang){

    if(lang=='jvc'){
      this.lang="nodejs";
    }
    else if(lang=='java'){
      this.lang="Springboot";
    }
    else if(lang=='python'){
      this.lang="nd/sp";
    }
  }

  factory backendLang(String lang){

    return backendLang._private(lang);
  }
}