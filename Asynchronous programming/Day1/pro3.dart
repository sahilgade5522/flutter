void fun1(){

  for(int i=1;i<6;i++){

    print("in fun1");
  }
}

void fun2(){

  for(int i=1;i<6;i++){

    print("in fun2");
  }
  Future.delayed(Duration(seconds:5),()=>print("fun2"));

    for(int i=1;i<6;i++){

    print("in fun2-new");
  }
}

void main(){

  print("main start");

  fun1();
  Future.delayed(Duration(seconds:5),()=>print('main fun'));
  fun2();

  print("end main");

}